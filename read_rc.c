#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <errno.h>		// errno
extern int errno;

#include <ctype.h>		// isspace

#define _XOPEN_SOURCE /* glibc2 needs this */
#include <time.h>

#ifdef DEBUG
#undef DEBUG
#define DEBUG(s) s
#else
#define DEBUG(s)
#endif

/*
** uncomment - take a string and remove the comment
**
** return - int
** return - 0 - if empty or starts with a comments
**              including whitespace lead comments
**          1 - if an uncommented string is returned
*/

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define NL      (char) 0x0A
#define CR      (char) 0x0D

#define COMMENT (char) 0x23

/*
** uncomment - Uncomment the original string and send it back in copy
**             also removes all whitespace
*/

int
uncomment(char *orig, char *copy) {
  int loc = 0;

  while(*orig) {
    if(*orig == NL || *orig ==  COMMENT || *orig == CR) break;

    // Don't copy/skip over the spaces
    if(!isspace(*orig)) {
      *copy = *orig;
      copy++;
      loc++;
    }

    orig++;

    DEBUG( if(loc > 100) { printf("Ouch!\n"); copy[0] = '\0'; return 0; });
  }

  *copy = '\0';

  return loc;
}

// Taken from: http://rosettacode.org/wiki/Rot-13#C

/*
** rot13 - performs the Caesar cipher on a string, see:
**         http://en.wikipedia.org/wiki/ROT13 for details
**
**         Yes I realize that trusting that the rot ptr is of the correct size
**         is dangerous
*/

void
rot13(char orig[], char rot[]) {
  int upper;

  while(*orig) {
    upper = toupper(*orig);

    if(upper >= 'A' && upper <= 'M') {
      *rot = *orig + 13;
    } else if(upper >= 'N' && upper <= 'Z') {
      *rot = *orig - 13;
    }

    ++orig;
    ++rot;
  }

  *rot = '\0';
}

int
main(int argc, char **argv) {
  char *ptr;

  int rtn = 0;

  char buf[255];
  char line[255];
  char variable[255];
  char value[255];

  double lat = 0.0;
  double lon = 0.0;
  int    tz  = 0;

  // =========================================================================


  /*
  ** Result: (A ) = (B)
  ** Result: () = ()
  ** Result: (A) = (B)
  ** Result: (A      ) = (B) // That's a tab after the A
  */

  // =========================================================================
  char fname[] = "z.rc";
  FILE *fp;

  // Open the file
  if((fp = fopen(fname, "r")) == NULL) {
    fprintf(stderr, "Can't open %s, errno: %d\n", fname, errno);
    exit(errno);
  }

  DEBUG(puts("Success!"));

  while(fgets(buf, sizeof(buf), fp) != NULL) {
    DEBUG(printf(" line: %s", buf));

    if(rtn = uncomment(buf, line) > 0) {
      DEBUG(printf("uline: %s -> ", line));

      rtn = sscanf(line, "%[^#=]=%s", &variable, &value );
      if(rtn) {
	printf("(%s) = (%s) [%d]\n", variable, value, rtn);

	if(strcmp("LAT", variable) == 0) {
	  //lat = atof(value);
	  lat = strtod(value, (char **) NULL);
	  if(lat) {
	    printf("LAT: %f\n", lat);

	  }
	}
	if(strcmp("LONG", variable) == 0) {
	  //lat = atof(value);
	  lon = strtod(value, (char **) NULL);
	  if(lat) {
	    printf("LONG: %f\n", lon);

	  }
	}
	if(strcmp("TZ", variable) == 0) {
	  //lat = atof(value);
	  tz = (int) strtol(value, (char **) NULL, 10);
	  if(lat) {
	    printf("TZ: %d\n", tz);

	  }
	}

      }

    }

  }
  fclose(fp);

  // Read the file a line at a time
  // skip the blank lines
  // skip the comment lines
  // split and remove comments
  // break down lines into valiable and value

  // =========================================================================

  rot13("frperg", line);
  printf("\nROT13: %s\n", line);

  exit(0);
}

