/*
** Sunset.c - computes the sunset and sends the output to stdout
*/

#include <stdio.h>
#include <stdlib.h>

extern double lat;
extern double lon;

int
main(int argc, char *argv[]) {
    int sunrh, sunrm, sunsh, sunsm;

    // Get the env settings for LAT and LONG
    char *latStr = getenv("LAT");
    if(latStr) {
	lat = strtod(latStr, NULL);
    }

    char *lonStr = getenv("LON");
    if(lonStr) {
	lon = strtod(lonStr, NULL);
    }
    
    sun(&sunrh, &sunrm, &sunsh, &sunsm);

    printf("%02d:%02d\n", sunsh, sunsm);
    printf("LAT:%s\n", latStr);
    printf("LON:%s\n", lonStr);

    return(0);
}
